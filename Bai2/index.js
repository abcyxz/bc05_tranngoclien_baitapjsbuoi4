/* Input:
Ô dropdown để user chọn thành viên


Step:
Step 1: tạo biến cho ô dropdown bằng ID
STep 2: xài if else cho bố, mẹ, anh, em. 
Nếu chọn bố thì chào bố, nếu chọn mẹ thì chào mẹ
Nếu chọn anh thì chào anh, nếu chọn em thì chào em 


Ouput:
Xin chào thành viên mà người dùng chọn*/






function sayHello(){
    var value=document.getElementById("txt-select").value;
    console.log('value: ', value);
    var msg="";
if (value=="B"){
    msg=("Xin chào Bố")
}
else if (value=="M"){
    msg=("Xin chào Mẹ")
}
else if (value=="A"){
    msg=("Xin chào anh")
}
else {
    msg=("Xin chào em")
}
document.getElementById("result").innerHTML=msg
}