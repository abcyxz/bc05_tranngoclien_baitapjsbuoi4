/* 
Input:
Số có 3 chữ số


Step:
Step 1: tạo biến chứa số
Step 2: xác định hàng trăm, hàng chục, hàng đơn vị trong 3 con số
Step 3: Xài switch cho từng trường hợp: hằng trăm đọc sao, chục đọc sao, đơn vị đọc sao.
Step 4: Xuất ra MH kết quả bằng 3 cái switch đó cộng lại


Output:
Đọc số đã nhập bằng text
*/











function docSo() {
    var number = document.getElementById("so").value * 1;
    console.log("number: ", number);
    (hangTram = Math.floor(number / 100)),
      (hangChuc = Math.floor((number % 100) / 10)),
      (hangDonVi = (number % 100) % 10),
      (a = "");
    b = "";
    c = "";
  
    switch (hangTram) {
      case 1:
        a = "một trăm";
        break;
      case 2:
        a = "hai trăm";
        break;
      case 3:
        a = "ba trăm";
        break;
      case 4:
        a = "bốn trăm";
        break;
      case 5:
        a = "năm trăm";
        break;
      case 6:
        a = "sáu trăm";
        break;
      case 7:
        a = "bảy trăm";
        break;
      case 8:
        a = "tám trăm";
        break;
      case 9:
        a = "chín trăm";
        break;
      default:
        alert("ko hợp lệ");
    }
    switch (hangChuc) {
      case 0:
        b = "lẻ";
      case 1:
        b = "mười";
        break;
      case 2:
        b = "hai mươi";
        break;
      case 3:
        b = "ba mươi";
        break;
      case 4:
        b = "bốn mươi";
        break;
      case 5:
        b = "năm mươi";
        break;
      case 6:
        b = "sáu mươi";
        break;
      case 7:
        b = "bảy mươi";
        break;
      case 8:
        b = "tám mươi";
        break;
      case 9:
        b = "chín mươi";
        break;
      default:
        alert("ko hợp lệ");
    }
    switch (hangDonVi) {
      case 0:
        c = "";
        break;
      case 1:
        c = "một";
        break;
      case 2:
        c = "hai";
        break;
      case 3:
        c = "ba";
        break;
      case 4:
        c = "bốn";
        break;
      case 5:
        c = "năm";
        break;
      case 6:
        c = "sáu";
        break;
      case 7:
        c = "bảy";
        break;
      case 8:
        c = "tám";
        break;
      case 9:
        c = "chín";
        break;
      default:
        alert("ko hợp lệ");
    }
    if (hangDonVi == 0 && hangChuc == 0) {
      document.getElementById("result").innerHTML = a;
    } else if (hangChuc == 0) {
      document.getElementById("result").innerHTML = a + "lẻ" + c;
    } else {
      document.getElementById("result").innerHTML = a + " " + b + " " + c;
    }
  }
  