/* Input:
Nhập tháng, năm


Step:
Step 1: Tạo 2 biến tương ứng với tháng, năm
Step 2: Check năm nhuần theo yêu cầu đề
STep 3: phân loại trường hợp 30 ngày, 31 ngày, 29 ngày, 28 ngày bằng if else 


Output:
Có bao nhiêu ngày trong tháng đó năm đó */











function tinhNgay() {
    
    var month = document.getElementById("thang").value * 1;
    console.log("month: ", month);
    var year = document.getElementById("nam").value * 1;
    console.log("year: ", year);
  
    var checkNamNhuan;
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
      checkNamNhuan = true;
      console.log("checkNamNhuan: ", checkNamNhuan);
    } else {
      checkNamNhuan = false;
      console.log("checkNamNhuan: ", checkNamNhuan);
    }
  
    if (
      month == 1 ||
      month == 3 ||
      month == 5 ||
      month == 7 ||
      month == 8 ||
      month == 10 ||
      month == 12
    ) {
      document.getElementById("result").innerHTML =
        "Tháng " + month + "năm" + year + "có 31 ngày";
    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
      document.getElementById("result").innerHTML =
        "Tháng " + month + "năm" + year + " có 30 ngày";
    } else if (month == 2 && checkNamNhuan == true) {
      document.getElementById("result").innerHTML =
        "Tháng 2" + "năm" + year + " có 29 ngày";
    } else if (month == 2 && checkNamNhuan == false) {
      document.getElementById("result").innerHTML =
        "Tháng 2" + "năm" + year + " có 28 ngày";
    }
  }
  