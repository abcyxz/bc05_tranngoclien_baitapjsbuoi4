/* Input:
3 dữ liệu: ngày, tháng, năm

Step:
Step 1: Tạo biến chứa 3 dữ liệu user nhập: ngày, tháng, năm
Step2: Check năm thuần bằng if else
Step 3: Xài if else cho các trường hợp đặc biệt, cụ thể thì code từng dòng theo để bài cho từng button

Output:
Số ngày/tháng/năm trên MH tùy theo User chọn nút hôm qua hay ngày mai.
*/










function homQua() {
    var day = document.getElementById("ngay").value * 1;
    console.log("day: ", day);
  
    var month = document.getElementById("thang").value * 1;
    console.log("month: ", month);
  
    var year = document.getElementById("nam").value * 1;
    console.log("year: ", year);
  
    var checkNamNhuan;
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
      checkNamNhuan = true;
      console.log("checkNamNhuan: ", checkNamNhuan);
    } else {
      checkNamNhuan = false;
      console.log("checkNamNhuan: ", checkNamNhuan);
    }
  
    var b;
    var c;
    var abcxyz;
    if (
      (day == 1 && month == 5) ||
      (day == 1 && month == 7) ||
      (day == 1 && month == 10) ||
      (day == 1 && month == 12)
    ) {
      b = "30/" + (month - 1) + year;
      console.log("b: ", b);
      document.getElementById("result").innerHTML = b;
    } else if (
      (day == 1 && month == 2) ||
      (day == 1 && month == 4) ||
      (day == 1 && month == 6) ||
      (day == 1 && month == 8) ||
      (day == 1 && month == 9) ||
      (day == 1 && month == 11)
    ) {
      document.getElementById("result").innerHTML =
        "31/" + (month - 1) + "/" + year;
    } else if (day == 1 && month == 3 && checkNamNhuan == true) {
      abcxyz = "29/2/" + year;
      console.log("abcxyz: ", abcxyz);
  
      document.getElementById("result").innerHTML = abcxyz;
    } else if (day == 1 && month == 3 && checkNamNhuan == false) {
      console.log("vv");
      document.getElementById("result").innerHTML = "28/2" + "/" + year;
    } else if (day == 1 && month == 1) {
      document.getElementById("result").innerHTML = "31/12/" + (year - 1);
    } else {
      //  dong nay luon chay
      document.getElementById("result").innerHTML =
        day - 1 + "/" + month + "/" + year;
    }
  }
  
  //
  
  function ngayMai() {
    var day = document.getElementById("ngay").value * 1;
    console.log("day: ", day);
  
    var month = document.getElementById("thang").value * 1;
    console.log("month: ", month);
  
    var year = document.getElementById("nam").value * 1;
    console.log("year: ", year);
  
    var checkNamNhuan;
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
      checkNamNhuan = true;
      console.log("checkNamNhuan: ", checkNamNhuan);
    } else {
      checkNamNhuan = false;
      console.log("checkNamNhuan: ", checkNamNhuan);
    }
    var d;
    /* var d
     */ if (
      (day == 31 && month == 1) ||
      (day == 31 && month == 3) ||
      (day == 31 && month == 5) ||
      (day == 31 && month == 7) ||
      (day == 31 && month == 8) ||
      (day == 31 && month == 10) ||
      (day == 31 && month == 12)
    ) {
      /*   d=(     "1/" + (month + 1) +("/") + year    )
      console.log('d: ', d); */
  
      document.getElementById("result").innerHTML =
        "1/" + (month + 1) + "/" + year;
    } else if (
      (day == 30 && month == 4) ||
      (day == 30 && month == 6) ||
      (day == 30 && month == 9) ||
      (day == 30 && month == 11)
    ) {
      document.getElementById("result").innerHTML =
        "1/" + (month + 1) + "/" + year;
    } else if (month == 2 && checkNamNhuan == true && day == 29) {
      document.getElementById("result").innerHTML = "1/3" + "/" + year;
    } else if (day == 31 && month == 12) {
      document.getElementById("result").innerHTML =
        (day == 1) + "/" + (month == 1) + "/" + (year + 1);
    } else if (month == 2 && checkNamNhuan == false && day == 28) {
      document.getElementById("result").innerHTML = "1/3" + "/" + year;
    } else {
      document.getElementById("result").innerHTML =
        day + 1 + "/" + month + "/" + year;
    }
  }
  